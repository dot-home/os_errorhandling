

#ifndef _OS_ERROR_HANDLER_H_
#define _OS_ERROR_HANDLER_H_

#include "OS_Config.h"
    
#if USE_OS_ERROR_HANDLING

/********************************* includes **********************************/
#include "OS_Faults.h"
#include "BaseTypes.h"

/***************************** defines / macros ******************************/


/****************************** type definitions *****************************/
typedef struct
{
    teErrorList eFaultCode;
    u8 ucSendTimeout;
    bool bHandlingOnLV;
}tSendErrorDelay;

typedef struct
{
    u16 uiRetryTimeOut;
    u16 uiDecreaseTimeout;
    u8  ucRetryCount;
    teErrorList eFaultCode;
}tRetry;

/***************************** global variables ******************************/


/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif

u16  OS_ErrorHandler_GetErrorTimeout(void);
void OS_ErrorHandler_OneSecRetryTimerTick(void);
void OS_ErrorHandler_HandleActualError(teErrorList eErrorName, bool bSetError);
bool OS_ErrorHandler_SendErrorMessage(void);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_ERROR_HANDLING

#endif /* _OS_ERROR_HANDLER_H_ */



/********************************* includes **********************************/
#include "OS_ErrorDebouncer.h"
#include "BaseTypes.h"
#include "HAL_System.h"

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266
#include <Arduino.h>
#endif

/***************************** defines / macros ******************************/
#define SLOT_NOT_FOUND          0xFF
#define NO_ERROR                eNoError
#define LAST_ENTRY              eErrorListLastEntry
#define NUMBER_OF_QUEUE_SLOTS   8u

/************************ local data type definitions ************************/
typedef struct
{
    u8 ucDebouncerTimer;        /* */
    u8 ucDebouncerLimit;        /* The limit of the fault until it's given to the error handler */
    u8 ucDebouncerCounter;      /* Counts the amount of appereances of this fault */
    u8 ucErrorCodeIndex;        /* The Fault-code name */
}tsErrorDebouncerData;

typedef struct
{
    u8 sErrorCodeInQueue[NUMBER_OF_QUEUE_SLOTS];
    u8 ucPutIndex;
    u8 ucGetIndex;
    s8 scMessagesInQueue;
}tsErrorQueue;


/************************* local function prototypes *************************/
static u8 ucGetDebouncerIndexOfError(u8 ucFaultCodeIndex);
static u8 ucGetFreeSlot(void);
static u8 ucCountOfErrors = 0;
static bool bIsFaultCodeValid(u8 ucFaultCodeIndex);


/************************* local data (const and var) ************************/
static tsErrorDebouncerData sErrorDebouncerData[NUMBER_OF_ERROR_SLOTS];
static tsErrorQueue sErrorQueue;


/************************ export data (const and var) ************************/
/****************************** local functions ******************************/

/**********************************************************
 * @brief   ucGetDebouncerIndexOfError - Search the debouncer array for an existing fault code.
 * @param   ucFaultCodeIndex - Fault code which should be searched for.
 * @return  ucReturnIndex - Returns the index number from the slot.
 **********************************************************/
static u8 ucGetDebouncerIndexOfError(u8 ucFaultCodeIndex)
{
    u8 ucReturnIndex = SLOT_NOT_FOUND;
    u8 ucSlotIndex;
    for(ucSlotIndex = 0; ucSlotIndex < NUMBER_OF_ERROR_SLOTS; ucSlotIndex++)
    {
        if(ucFaultCodeIndex == sErrorDebouncerData[ucSlotIndex].ucErrorCodeIndex)
        {
            ucReturnIndex = ucSlotIndex;
            break;
        }
    }
    return ucReturnIndex;
}


/**********************************************************
 * @brief   ucGetFreeSlot - Search the debouncer array for an free slot.
 * @param   none 
 * @return  ucReturnFreeSlotIndex - Returns the index number from the slot.
 **********************************************************/
static u8 ucGetFreeSlot(void)
{
    u8 ucReturnFreeSlotIndex = SLOT_NOT_FOUND;
    u8 ucSlotIndex;
    for(ucSlotIndex = 0; ucSlotIndex < NUMBER_OF_ERROR_SLOTS; ucSlotIndex++)
    {
        /* Check slots with an error code of NO_ERROR */
        if(sErrorDebouncerData[ucSlotIndex].ucErrorCodeIndex == NO_ERROR)
        {
            ucReturnFreeSlotIndex = ucSlotIndex;
            break;
        }
    }
    return ucReturnFreeSlotIndex;
}

/**********************************************************
 * @brief   bIsFaultCodeValid - Check if FaultCodeIndex is in a valid range.
 *
 * @param   ucFaultCodeIndex - Code index which should be checked.
 * 
 * @return  bReturnValue - Boolean if the code is in range.
 **********************************************************/
static bool bIsFaultCodeValid(u8 ucFaultCodeIndex)
{
    return (NO_ERROR < ucFaultCodeIndex && ucFaultCodeIndex <= LAST_ENTRY) ? true : false;
}


/**********************************************************
 * @brief   DeleteErrorInDebouncer - Delete  cured error from the error debouncer.
 *                                      Only possible when the error counter is greater than zero.
 *                                      This function deletes an error, when it doesn't occure anymore.
 *
 * @param   ucFaultCodeIndex - Faultcode which should be checked.
 **********************************************************/
static void DeleteErrorInDebouncer(u8 ucFaultCodeIndex)
{        
    if(OS_ErrorDebouncer_bIsAnyErrorSet()
       && bIsFaultCodeValid(ucFaultCodeIndex))
    {
        /* Check debouncer array for existing error */
        u8 ucSlotIndex = ucGetDebouncerIndexOfError(ucFaultCodeIndex);
        
        if(ucSlotIndex != SLOT_NOT_FOUND)
        {
            /* Reset error debouncer slot */
            sErrorDebouncerData[ucSlotIndex].ucDebouncerLimit = 0;
            sErrorDebouncerData[ucSlotIndex].ucDebouncerCounter = 0;
            sErrorDebouncerData[ucSlotIndex].ucErrorCodeIndex = NO_ERROR;
            
            /* Decrement error counter */
            ucCountOfErrors--;
        }
    }
}


/************************ externally visible functions ******************************************************************************************************************/

/**********************************************************
 * @brief   OS_ErrorDebouncer_Initialize - Initialize the error debouncer.
 * @param   none
 **********************************************************/
void OS_ErrorDebouncer_Initialize(void)
{
    u8 ucSlotIndex;
    ucCountOfErrors = 0;
    for(ucSlotIndex = 0; ucSlotIndex < NUMBER_OF_ERROR_SLOTS; ucSlotIndex++)
    {       
        tsErrorDebouncerData* psErrorDebouncerData = &sErrorDebouncerData[ucSlotIndex];
        psErrorDebouncerData->ucDebouncerTimer = 0;
        psErrorDebouncerData->ucDebouncerLimit = 0;
        psErrorDebouncerData->ucDebouncerCounter = 0;
        psErrorDebouncerData->ucErrorCodeIndex = NO_ERROR;
    }
}


/**********************************************************
 * @brief   OS_ErrorDebouncer_SetError - Save actual error in the error debouncer.
 *                                    Only possible when the error counter is smaller than the
 *                                    amount of error slots.
 *                                    Shall be called by a timer (opt 50ms).
 *
 * @param   ucFaultCodeIndex - Faultcode which should be checked.
 **********************************************************/
void OS_ErrorDebouncer_SetErrorInDebouncer(void)
{
    //Pointer to the error queue
    tsErrorQueue* psErrorQueue = &sErrorQueue;
    
    if(psErrorQueue->scMessagesInQueue)
    {
        //Get all errors from error queue
        while(psErrorQueue->scMessagesInQueue)
        {
            if( ucCountOfErrors < NUMBER_OF_ERROR_SLOTS
                && bIsFaultCodeValid(psErrorQueue->sErrorCodeInQueue[psErrorQueue->ucGetIndex]))
            {
                /* Check debouncer array for existing error */
                u8 ucSlotIndex = ucGetDebouncerIndexOfError(psErrorQueue->sErrorCodeInQueue[psErrorQueue->ucGetIndex]);

                if(ucSlotIndex == SLOT_NOT_FOUND)
                {
                    /* Get a free slot */
                    ucSlotIndex = ucGetFreeSlot();
                    
                    /* When the error counter isn't correct and there aren't any free slots
                    *  skip this routine
                    */
                    if(ucSlotIndex == SLOT_NOT_FOUND)
                    {
                        OS_EVT_PostEvent(eEvtErrorHandling, eEvtParam_ErrorInvalidSlot,0);
                        continue;
                    }
                    
                    /* Save error in new slot */
                    sErrorDebouncerData[ucSlotIndex].ucErrorCodeIndex = psErrorQueue->sErrorCodeInQueue[psErrorQueue->ucGetIndex];
                    sErrorDebouncerData[ucSlotIndex].ucDebouncerLimit = OS_Faults_GetDebouncerLimit((teErrorList)sErrorDebouncerData[ucSlotIndex].ucErrorCodeIndex);
                    
                    /* Increment error counter */
                    ucCountOfErrors++; 
                }

                /* Increment debouncer calls for this error */
                if(sErrorDebouncerData[ucSlotIndex].ucDebouncerCounter <= sErrorDebouncerData[ucSlotIndex].ucDebouncerLimit)
                {
                    sErrorDebouncerData[ucSlotIndex].ucDebouncerCounter++;
                }
                
                /* Reset debouncer timer for this error (delay the delete process)*/
                sErrorDebouncerData[ucSlotIndex].ucDebouncerTimer = 0;

                /* Decrement queue counter */
                psErrorQueue->scMessagesInQueue--;
                
                /* Increment get index */
                if(++psErrorQueue->ucGetIndex == _countof(psErrorQueue->sErrorCodeInQueue))
                {
                    psErrorQueue->ucGetIndex = 0;
                }
            }
            else
            {
                OS_EVT_PostEvent(eEvtErrorHandling, eEvtParam_ErrorToManyErrors, 0);
            }
        }
    }
}


/**********************************************************
 * @brief   OS_ErrorDebouncer_ResetDebouncer - Is the same function as Debouncer initialize.
 *
 * @param   none
 **********************************************************/
void OS_ErrorDebouncer_ResetDebouncer(void)
{
    OS_ErrorDebouncer_Initialize();
}


/**********************************************************
 * @brief   OS_ErrorDebouncer_bIsErrorSet - Checks the debouncer slots for existing error.
 *
 * @param   ucFaultCodeIndex - Faultcode which should be checked.
 **********************************************************/
bool OS_ErrorDebouncer_bIsErrorSet(u8 ucFaultCodeIndex)
{
    return (ucGetDebouncerIndexOfError(ucFaultCodeIndex) == SLOT_NOT_FOUND) ? false : true;   
}


/**********************************************************
 * @brief   OS_ErrorDebouncer_bIsErrorSet - Checks the debouncer slots for existing error.
 *
 * @param   ucFaultCodeIndex - Faultcode which should be checked.
 **********************************************************/
bool OS_ErrorDebouncer_bIsAnyErrorSet(void)
{   
    return ucCountOfErrors ? true : false;
}


/**********************************************************
 * @brief   OS_ErrorDebouncer_DebounceErrors - Primary method to debounce errors. When the debouncer counter reached its limit
 *                                          an error event will be generated once. While the error occurs, the debounce timer will
 *                                          be frequently set to zero in "OS_ErrorDebouncer_SetErrorInDebouncer()". When there isn't any
 *                                          error, the timer will overflow and delete the error from the debouncer.
 *                                          If the error event should be started frequently, the ucDebouncerCounter should be set to zero.
 *                                          Will be called every 250ms.
 *
 * @param   none
 **********************************************************/
void OS_ErrorDebouncer_DebounceErrors(u16 uiElapsedTime)
{
    /*Check if any error is set */
    if(OS_ErrorDebouncer_bIsAnyErrorSet())
    {
        u8 ucSlotIndex;
        /* Check all slots for an error */
        for(ucSlotIndex = 0; ucSlotIndex < NUMBER_OF_ERROR_SLOTS; ucSlotIndex++)
        {
            tsErrorDebouncerData* psErrorDebouncerData = &sErrorDebouncerData[ucSlotIndex];

            if(psErrorDebouncerData->ucErrorCodeIndex != NO_ERROR)
            {
                /* Check if error counter has reached the debouncer limit */
                if( psErrorDebouncerData->ucDebouncerCounter >= psErrorDebouncerData->ucDebouncerLimit)
                {
                    tErrorPackage sErrorPackage;
                    u32 ulErrorParam = 0;
                    sErrorPackage.eFaultCode = psErrorDebouncerData->ucErrorCodeIndex;
                    sErrorPackage.bHandleError = true;  //Always handle own errors
                    sErrorPackage.ucErrorSource = 0;    //Own address

                    OS_ErrorDebouncer_GetParamFromErrorPackage(&ulErrorParam, &sErrorPackage);
                    OS_EVT_PostEvent(eEvtError, 0, ulErrorParam);
                }
                
                /* Increment error timer till overflow */
                psErrorDebouncerData->ucDebouncerTimer += uiElapsedTime;

                /* Check if fault can be cleared because of a timeout */
                if(psErrorDebouncerData->ucDebouncerTimer >= DEBOUNCER_CONFIG_TIMER)
                {
                    //Delete error and set timer back to zero
                    DeleteErrorInDebouncer(psErrorDebouncerData->ucErrorCodeIndex);
                    psErrorDebouncerData->ucDebouncerTimer = 0;
                }
            }
        }
    }
}


/**********************************************************
* @brief    OS_ErrorDebouncer_PutErrorInQueue - Puts an fault code in the error queue
* @param    ucFaultCodeIndex - Faultcode which should be put in the queue.
* @param    bSet - Set "true" for setting the error code and "false" to delete it.
 **********************************************************/
void OS_ErrorDebouncer_PutErrorInQueue(u8 ucFaultCodeIndex)
{
    //Disable global interrupts
    HAL_System_EnterCriticalSection();
    
    //Point with an pointer to the first queue index
    tsErrorQueue* psErrorQueue = &sErrorQueue;

    //Set fault code and write/delete switch
    psErrorQueue->sErrorCodeInQueue[psErrorQueue->ucPutIndex] = ucFaultCodeIndex;
    
    //Increment error-in-queue counter while queue isn't full
    if(psErrorQueue->scMessagesInQueue < (s8)NUMBER_OF_QUEUE_SLOTS)
    {
        psErrorQueue->scMessagesInQueue++;
    }
    
    //If the put-index reaches the end of the queue, the put-index should start again with zero.
    if(++psErrorQueue->ucPutIndex == _countof(psErrorQueue->sErrorCodeInQueue))
    {
        psErrorQueue->ucPutIndex = 0;
    }

    //Enable global interrupts
    HAL_System_LeaveCriticalSection();
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.05.2018
\brief      Returns the u32 value for the error package
\param      tErrorPackage - error package which should be casted to an u32
\param      u32* pulReturnErrorPackage - u32 value which represent the error package
\return     Pointer to the u32 value.
***********************************************************************************/
void OS_ErrorDebouncer_GetParamFromErrorPackage(u32* pulReturnErrorPackage, tErrorPackage* psErrorPackage)
{
    if(pulReturnErrorPackage && psErrorPackage)
    {
        *pulReturnErrorPackage |= psErrorPackage->ucErrorSource;
        *pulReturnErrorPackage |= (psErrorPackage->bHandleError << 8);
        *pulReturnErrorPackage |= (psErrorPackage->eFaultCode << 16);  
//        memcpy(pulReturnErrorPackage, psErrorPackage, sizeof(*pulReturnErrorPackage)); 
    }
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.05.2018
\brief      Returns the error package from an u32 value.
\param      u32* pulErrorPaked - Pointer to the value which should be casted to an error package 
\param      tErrorPackage* psReturnErrorPackage - Returning Error package
\return     Pointer to the unpacked error package.
***********************************************************************************/
void OS_ErrorDebouncer_GetErrorPackageFromParam(u32* pulErrorPackage, tErrorPackage* psReturnErrorPackage)
{
    if(pulErrorPackage && psReturnErrorPackage)
    {
        psReturnErrorPackage->ucErrorSource = (u8)*pulErrorPackage;
        psReturnErrorPackage->bHandleError = (u8)(*pulErrorPackage >> 8);
        psReturnErrorPackage->eFaultCode = (teErrorList)(*pulErrorPackage >> 16);
        
//        memcpy(psReturnErrorPackage, pulErrorPackage, sizeof(*psReturnErrorPackage));    
    }
}

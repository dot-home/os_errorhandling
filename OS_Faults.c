


/********************************* includes **********************************/
#include "OS_Faults.h"

/***************************** defines / macros ******************************/

/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/
const tsFaults sErrorArray[] = 
{
    #ifdef OS_ERROR_LIST
        #define ERROR(ErrorName, ErrorCode, Priority, DebounceCnt) {ErrorCode, Priority, DebounceCnt},
            OS_ERROR_LIST
        #undef ERROR
    #endif
    #ifdef USER_ERROR_LIST
        #define ERROR(ErrorName, ErrorCode, Priority, DebounceCnt) {ErrorCode, Priority, DebounceCnt},
            USER_ERROR_LIST
        #undef ERROR
    #endif
};


/****************************** local functions ******************************/


/************************ externally visible functions ***********************/

//***************************************************************************
/*!
\author     KraemerE
\date       16.04.2018
\brief      Returns the code index from the error value
\return     eFaultCode - Returns the enum from the list
\param      uiErrorCode - Two byte error code from fault list
******************************************************************************/
teErrorList Faults_GetErrorIndexFromValue(u16 uiErrorCode)
{
    teErrorList eFaultCode = eNoError;
    
    u16 uiIndex;
    for(uiIndex = eErrorListLastEntry; uiIndex--;)
    {
        if(sErrorArray[uiIndex].uiErrorCode == uiErrorCode)
        {
            eFaultCode = (teErrorList)uiIndex;
            break;
        }
    }
    
    return eFaultCode;
}

//***************************************************************************
/*!
\author     KraemerE
\date       16.04.2018
\brief      Returns the amount of debounces for this fault
\return     ucDebouncerLimit - The debounce limit for this fault
\param      eErrorName - The error name of the debounce count
******************************************************************************/
u8 OS_Faults_GetDebouncerLimit(teErrorList eErrorName)
{
    u8 ucDebouncerLimit = FAULTS_DEBOUNCE_CNT_DEFAULT;

    if(eErrorName != eNoError)
    {
        ucDebouncerLimit = sErrorArray[eErrorName].ucDebounceCnt;
    }

    return ucDebouncerLimit;
}

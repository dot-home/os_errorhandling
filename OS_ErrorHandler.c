/**
 * @file    OS_ErrorHandler.c
 * @brief   Handles generic faults which are OS depenent.
            Also uses user specific fault handling.
 */

/********************************* includes **********************************/
#include "OS_ErrorHandler.h"
#include "OS_EventManager.h"
#include "Project_Config.h"

#ifdef USER_ERROR_LIST
#include "ErrorHandler.h"   //User Error handler
#endif

/***************************** defines / macros ******************************/
#define RETRY_COUNTS                5   // Is equal to 31seconds
#define NUMBER_OF_SAVED_ERRORS      10
#define DELAY_IN_MILLISECONDS       1000
#define DELAY_FOR_ERROR_MESSAGE     DELAY_IN_MILLISECONDS/50    //Every 50Ms the counter will be decremented.

/************************ local data type definitions ************************/

/************************* local function prototypes *************************/
static void PutErrorMessageInBuffer(teErrorList eFaultCode);
static void SetRetryTimer(tRetry* psRetry, teErrorList eFaultCode);

/************************* local data (const and var) ************************/
static tSendErrorDelay sSendErrorDelay[NUMBER_OF_SAVED_ERRORS];
static tRetry sRetryData;
static u8 ucErrorInBuffer = 0;

/************************ export data (const and var) ************************/

/****************************** local functions ******************************/

//********************************************************************************
/*!
 \author     KraemerE
 \date       16.04.2018
 \brief      Puts appearing errors in queue with an timed delay, to prevent an message overflow.
 \param      eFaultCode - Faultcode which should be saved.
 \return     none
 ***********************************************************************************/
static void PutErrorMessageInBuffer(teErrorList eFaultCode)
{
    bool bErrorFound = false;
    
    /* Check if faultcode is already saved */
    u8 ucErrorIdx;
    for(ucErrorIdx = NUMBER_OF_SAVED_ERRORS; ucErrorIdx--;)
    {
        if(sSendErrorDelay[ucErrorIdx].eFaultCode == eFaultCode)
        {
            bErrorFound = true;
            break;
        }
    }
    
    /* If error wasn't found check for free slot */
    if(!bErrorFound)
    {
        for(ucErrorIdx = NUMBER_OF_SAVED_ERRORS; ucErrorIdx--;)
        {
            if(sSendErrorDelay[ucErrorIdx].eFaultCode == eNoError)
            {
                sSendErrorDelay[ucErrorIdx].eFaultCode = eFaultCode;
                sSendErrorDelay[ucErrorIdx].ucSendTimeout = DELAY_FOR_ERROR_MESSAGE;
                ucErrorInBuffer++;
                break;
            }
        }
    }
}

//********************************************************************************
/*!
 \author     KraemerE (Ego)
 \date       21.11.2018
 \brief      Sets the retry time for a tRetry-structure
 \param      eFaultCode - Faultcode which should be saved.
 \param      psRetry - Pointer to the retry structure which should be set.
 \return     none
 ***********************************************************************************/
static void SetRetryTimer(tRetry* psRetry, teErrorList eFaultCode)
{
    /* Set retry counter*/
    if(psRetry->uiRetryTimeOut == 0 && psRetry->ucRetryCount < RETRY_COUNTS)
    {
        psRetry->uiRetryTimeOut = (1 << ++(psRetry->ucRetryCount)) - 1;
        psRetry->uiDecreaseTimeout = psRetry->uiRetryTimeOut;
        
        if(eFaultCode != eNoError)
            psRetry->eFaultCode = eFaultCode;
        
    }
    else if(psRetry->ucRetryCount == RETRY_COUNTS)
    {
        psRetry->uiRetryTimeOut = (1 << psRetry->ucRetryCount) - 1;
        
        if(eFaultCode != eNoError)
            psRetry->eFaultCode = eFaultCode;
    }
}

//********************************************************************************
/*!
 \author     KraemerE
 \date       14.06.2021
 \brief      Decreases the retry count step by step with a timeout.
             Helps to unblock a fault which hasn't occured anymore.
 \param      psRetry - Pointer to the retry structure which should be decreased
 \return     none
 ***********************************************************************************/
static void DecreaseRetryTimer(tRetry* psRetry)
{
    /* When a retry count was set and the timeout has occured. start with the decrease */
    if(psRetry->uiRetryTimeOut == 0 && psRetry->ucRetryCount > 0)
    {
        /* WHen the decrease timeout has been reached */
        if(psRetry->uiDecreaseTimeout == 0)
        {
            --psRetry->ucRetryCount;
            
            /* Restart the decrease timeout */
            psRetry->uiDecreaseTimeout = (1 << psRetry->ucRetryCount);
        }
        else
        {
            --psRetry->uiDecreaseTimeout;
        }
    }
}

/************************ externally visible functions ***********************/

//********************************************************************************
/*!
 \author     KraemerE
 \date       16.04.2018
 \brief      Sends time based error messages to POD. Time base should be 50ms.
             Timeout is set to DELAY_FOR_ERROR_MESSAGE (= 350ms)
 \return     none
 ***********************************************************************************/
bool OS_ErrorHandler_SendErrorMessage(void)
{
    static u8 ucSendIndex = 0;
    bool bMessageSend = false;
    
    if(ucErrorInBuffer)
    {
        u8 ucIndex = 0;
        u8 ucLoopCounter = 0;
        
        /* Decrement timeout counter for all entrys */
        for(ucIndex = NUMBER_OF_SAVED_ERRORS; ucIndex--;)
        {
            if(sSendErrorDelay[ucIndex].ucSendTimeout > 0)
            {
                sSendErrorDelay[ucIndex].ucSendTimeout--;
            }
        }
        
        /* Use bool to indicate if error was found */
        for(; ucSendIndex < NUMBER_OF_SAVED_ERRORS; ucSendIndex++)
        {
            /* Increment loop counter to prevent an while */
            if(++ucLoopCounter >= NUMBER_OF_SAVED_ERRORS)
            {
                /* Complete buffer checked without succsses, leave the loop */
                bMessageSend = false;
                break;
            }
            
            /* Check if entry is an error */
            if(sSendErrorDelay[ucSendIndex].eFaultCode != eNoError)
            {
                /* If timeout is reached send message to LV */
                if(sSendErrorDelay[ucSendIndex].ucSendTimeout == 0)
                {
                    u16 uiErrorCode = sErrorArray[sSendErrorDelay[ucSendIndex].eFaultCode].uiErrorCode;
                    
                    /* Put event in queue */
                    OS_EVT_PostEvent(eEvtSendError, uiErrorCode, 0);
                    bMessageSend = true;
                    
                    /* Clear entry */
                    sSendErrorDelay[ucSendIndex].bHandlingOnLV = false;
                    sSendErrorDelay[ucSendIndex].eFaultCode = eNoError;
                    sSendErrorDelay[ucSendIndex].ucSendTimeout = 0;
                    
                    /* Decrement buffer counter */
                    ucErrorInBuffer--;
                }
            }
            
            /* Set the index to the start, when the end is reached */
            if(ucSendIndex == (NUMBER_OF_SAVED_ERRORS - 1))
            {
                ucSendIndex = 0;
            }
            
            /* Increment the send index and stop the for-loop when a message was send */
            if(bMessageSend)
            {
                ucSendIndex++;
                break;
            }
        }
    }
    
    return bMessageSend;
}

//********************************************************************************
/*!
 \author     KraemerE
 \date       02.02.2018
 \param      none
 \brief      Returns the timeout of the pending error.
 \return     u16 - Returns the timeout which is pending
 ***********************************************************************************/
u16 OS_ErrorHandler_GetErrorTimeout(void)
{
    return sRetryData.uiRetryTimeOut;
}

//********************************************************************************
/*!
 \author     KraemerE (Ego)
 \date       10.10.2017
 \param      none
 \brief      Decrement every second the retry timer.
 \return     none.
 ***********************************************************************************/
void OS_ErrorHandler_OneSecRetryTimerTick(void)
{
    /* While retry timer is running decrement the timeout 
     and put the error message in the buffer */
    if(sRetryData.uiRetryTimeOut)
    {
        sRetryData.uiRetryTimeOut--;
        PutErrorMessageInBuffer(sRetryData.eFaultCode);
    }
    else
    {
        sRetryData.uiRetryTimeOut = 0;
        
        /* Start to decrease the retry timeout */
        DecreaseRetryTimer(&sRetryData);
    }
}

//***************************************************************************
/*!
\author     KraemerE
\date       13.12.2017
\brief      Handles explicit errors in dependency of the fault code
\return     void
\param      eFaultCode - Faultcode name which shall be handled
\param      bSetError - True when the fault shall be handled and send. False
                        when the fault shall only be send.
 ******************************************************************************/
void OS_ErrorHandler_HandleActualError(teErrorList eErrorName, bool bSetError)
{
    if(bSetError)
    {
        bool bUserError = false;
        /* Check for user errors */
        #ifdef USER_ERROR_LIST
            /* When user error was already handled, set to true */
            bUserError = ErrorHandler_HandleActualError(eErrorName, bSetError);
        #endif

        if(bUserError == false)
        {
            switch(eErrorName)
            {
                case eFlashCRCInvalid:
                {
                    OS_EVT_PostEvent(eEvtFlashClearAll,0,0);
                    break;
                }
                
                /* Currently all faults are handled with the same handling */
                //case ePinFault:
                //case eInputVoltageFault:
                //case eOutputVoltageFault:
                //case eCommunicationFault:
                //case eOverCurrentFault:
                //case eOverTemperatureFault:
                //case eLoadMissingFault:
                default:
                {
                    /* Start the timeout for the locked regulation */
                    SetRetryTimer(&sRetryData, eErrorName);

                    /* Request a lower output voltage */
                    //lower output power is checked with "GetErrorTimeout"
                }
                    break;
            }
        }
        
        /* Check fault priority */
        if(sErrorArray[eErrorName].ucPriority == 1)
        {
            //Highest priority detected. Initialize reset within a second
            OS_EVT_PostEvent(eEvtExecuteSoftwareReset, 0, 0);
        }
    }

    /* Put error in Buffer */
    PutErrorMessageInBuffer(eErrorName);
}

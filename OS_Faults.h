//******************************************************************************
/*!
\author     KraemerE
\date       27.07.2017

\brief      Fault list collection

********************************************************************************    
*/

#ifndef _OS_FAULTS_H_
#define _OS_FAULTS_H_
    
#include "OS_Config.h"
#if USE_OS_ERROR_HANDLING
    
/********************************* includes **********************************/
#include "BaseTypes.h"  
#include "Project_Config.h"
    
#if defined __CC_ARM
    #pragma pack(push, 1)
#elif defined __GNUC__
    #pragma pack(push, 1)
#endif

/***************************** defines / macros ******************************/
#define OUTPUT_OK      0
#define OUTPUT_SHORTED 1
#define OUTPUT_OPEN    2
#define HVM_NODE_FAULT 3

#define FAULTS_DEBOUNCE_CNT_DEFAULT     4u   //Amount of times the fault shall occure for the debouncer until its put to the error handler

//Use of X-Macros for defining errors
/*              Error name                          |  Fault Code    |   Priority   |         Debouncer Count          */
#define OS_ERROR_LIST \
   ERROR(   eNoError                                 ,    0x0000      ,       0     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   ePinFault                                ,    0x1000      ,       2     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   ePmwFault                                ,    0x1001      ,       2     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   eInputVoltageFault                       ,    0x1002      ,       1     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   eCommunicationFault                      ,    0x1004      ,       2     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   eMessageCrcFault                         ,    0x1006      ,       3     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   eOverTemperatureFault                    ,    0x1008      ,       2     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )\
   ERROR(   eUnknownReturnValue                      ,    0x100B      ,       1     ,       1                          )\
   ERROR(   eInvalidPointerAccess                    ,    0x100C      ,       1     ,       1                          )\
   ERROR(   eFlashCRCInvalid                         ,    0x1010      ,       2     ,       1                          )\
   ERROR(   eFlashDataToBig                          ,    0x1011      ,       1     ,       1                          )\
   ERROR(   eFlashProtected                          ,    0x1012      ,       1     ,       1                          )\
   ERROR(   eFlashAddrInvalid                        ,    0x1013      ,       1     ,       1                          )\
   ERROR(   eTimerInvalidTimer                       ,    0x1020      ,       2     ,       1                          )\
   ERROR(   eTimerInvalid                            ,    0x1021      ,       2     ,       1                          )\
   ERROR(   eStateMachineError_EntryNxtState         ,    0x1022      ,       1     ,       1                          )\
   ERROR(   eStateMachineError_RootNxtState          ,    0x1023      ,       1     ,       1                          )\
   ERROR(   eStateMachineError_ExitNxtState          ,    0x1024      ,       1     ,       1                          )\
   ERROR(   eStateMachineError_NoState               ,    0x1025      ,       1     ,       1                          )\
   ERROR(   eStateMachineError_InvalidRequest        ,    0x1026      ,       1     ,       1                          )\
   ERROR(   eSoftwareTimer_TimerLimit                ,    0x1027      ,       1     ,       1                          )\
   ERROR(   eSoftwareTimer_InvalidRequest            ,    0x1028      ,       1     ,       1                          )\
   ERROR(   eEventError_NotProcessed                 ,    0x1029      ,       1     ,       1                          )\
   ERROR(   eOsTimerCreateFault                      ,    0x102A      ,       1     ,       1                          )\
   ERROR(   eOsTimerDeleteFault                      ,    0x102B      ,       1     ,       1                          )\
   ERROR(   eOsComm_ResenBufferFault                 ,    0x102C      ,       1     ,       1                          )\
   ERROR(   eOsComm_SendBufferFault                  ,    0x102D      ,       1     ,       1                          )\
   ERROR(   eOsMemory_InvalidFree                    ,    0x102E      ,       1     ,       1                          )\
   ERROR(   eOsMemory_InvalidCreate                  ,    0x102F      ,       1     ,       1                          )\
//ERROR(   eErrorListLastEntry                      ,    0xFFFF      ,       3     ,      FAULTS_DEBOUNCE_CNT_DEFAULT )

/****************************** type definitions *****************************/
// Generate an enum list for the error list
typedef enum
{
    #ifdef OS_ERROR_LIST
        #define ERROR(ErrorName, ErrorCode, Priority, DebounceCnt) ErrorName,
            OS_ERROR_LIST
        #undef ERROR
    #endif
    #ifdef USER_ERROR_LIST
        #define ERROR(ErrorName, ErrorCode, Priority, DebounceCnt) ErrorName,
            USER_ERROR_LIST
        #undef ERROR
    #endif
    eErrorListLastEntry
}teErrorList;


typedef struct
{
    u16 uiErrorCode;
    u8  ucPriority;
    u8  ucDebounceCnt;
}tsFaults;

/***************************** global variables ******************************/
// Use the uiError array for the listing
extern const tsFaults sErrorArray[];

#if defined __CC_ARM
    #pragma pack(pop)
#elif defined __GNUC__
    #pragma pack(pop)
#endif

/************************ externally visible functions ***********************/

#ifdef __cplusplus
extern "C"
{
#endif

teErrorList OS_Faults_GetErrorIndexFromValue(u16 uiErrorCode);
u8 OS_Faults_GetDebouncerLimit(teErrorList eErrorName);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_ERROR_HANDLING

#endif //_OS_FAULTS_H_



#ifndef ERROR_DEBOUNCER
#define ERROR_DEBOUNCER
    
#include "OS_Config.h"
#if USE_OS_ERROR_HANDLING
    
/********************************* includes **********************************/

#include "BaseTypes.h"
#include "OS_EventManager.h"
#include "OS_Faults.h"

#ifdef __cplusplus
extern "C"
{
#endif
/***************************** defines / macros ******************************/
#ifndef NUMBER_OF_ERROR_SLOTS
    #define NUMBER_OF_ERROR_SLOTS           10
#endif

#ifndef DEBOUNCER_CONFIG_TIMER
    #define DEBOUNCER_CONFIG_TIMER          1250   //Time in milliseconds until the fault should be cleared because of a timeout
#endif

/****************************** type definitions *****************************/
#ifdef CY_PROJECT_NAME
    typedef struct
    {
        teErrorList eFaultCode;
        bool bHandleError;
        u8 ucErrorSource;
    }tErrorPackage;
#else
    typedef struct
    {
        u8 eFaultCode;
        bool bHandleError;
        u8 ucErrorSource;
    }tErrorPackage;
#endif

/***************************** global variables ******************************/
/************************ externally visible functions ***********************/
void OS_ErrorDebouncer_Initialize(void);
void OS_ErrorDebouncer_SetErrorInDebouncer(void);
void OS_ErrorDebouncer_ResetDebouncer(void);
void OS_ErrorDebouncer_DebounceErrors(u16 uiElapsedTime);
bool OS_ErrorDebouncer_bIsErrorSet(u8 ucFaultCodeIndex);
bool OS_ErrorDebouncer_bIsAnyErrorSet(void);
void OS_ErrorDebouncer_PutErrorInQueue(u8 ucFaultCodeIndex);
void OS_ErrorDebouncer_GetParamFromErrorPackage(u32* pulReturnErrorPackage, tErrorPackage* psErrorPackage);
void OS_ErrorDebouncer_GetErrorPackageFromParam(u32* pulErrorPackage, tErrorPackage* psReturnErrorPackage);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_ERROR_HANDLING

#endif /* ERROR_DEBOUNCER */
